package med.voll.api.entity.medico;

public record DatosListaMedico(
        String nombre,
        String email,
        String crm,
        Especialidad especialidad
) {

    public DatosListaMedico(Medico medico){
        this(medico.getNombre(), medico.getEmail(), medico.getCrm(), medico.getEspecialidad());
    }
}
