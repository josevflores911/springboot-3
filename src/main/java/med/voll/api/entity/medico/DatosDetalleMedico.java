package med.voll.api.entity.medico;

import med.voll.api.entity.direccion.Direccion;

public record DatosDetalleMedico(Long id, String nombre,String email, String crm,String telefono, Especialidad especialidad, Direccion direccion) {
    public DatosDetalleMedico(Medico medico){
        this(medico.getId(), medico.getNombre(), medico.getEmail()
                , medico.getCrm(),medico.getTelefono(),medico.getEspecialidad(),medico.getDireccion());
    }
}
