package med.voll.api.entity.medico;

import jakarta.validation.Valid;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import med.voll.api.entity.direccion.DatosDireccion;

public record DatosRegistroMedico(
        @NotBlank
        String nombre,
        @NotBlank
        @Email
        String email,

        String telefono,
        @NotBlank
        //@Pattern(regexp="\\d{4,6}")check this
        String crm,
        @NotNull
        Especialidad especialidad,
        @NotNull
        @Valid
        DatosDireccion datosDireccion) {
}
