package med.voll.api.entity.medico;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import med.voll.api.entity.direccion.Direccion;

@Table(name="medicos")
@Entity(name="Medico")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of="id")
public class Medico {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private String email;
    private String telefono;
    private String crm;

    @Enumerated(EnumType.STRING)
    private Especialidad especialidad;

    @Embedded
    private Direccion direccion;

    private Boolean activo;

    public Medico(DatosRegistroMedico datosRegistroMedico) {
        this.activo=true;
        this.nombre=datosRegistroMedico.nombre();
        this.email=datosRegistroMedico.email();
        this.telefono= datosRegistroMedico.telefono();
        this.crm=datosRegistroMedico.crm();
        this.especialidad=datosRegistroMedico.especialidad();
        this.direccion=new Direccion(datosRegistroMedico.datosDireccion());
    }

    public void actualizarInformaciones(DatosActualizacaoMedico datos) {
        if(datos.nombre()!=null)
            this.nombre=datos.nombre();

        if(datos.telefono()!=null)
            this.telefono=datos.telefono();

        if(datos.direccion()!=null)
            this.direccion.actualizarDireccion(datos.direccion());
    }

    public void eliminar() {
        this.activo=false;
    }
}
