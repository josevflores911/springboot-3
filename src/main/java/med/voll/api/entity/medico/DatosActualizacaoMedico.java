package med.voll.api.entity.medico;

import jakarta.validation.constraints.NotNull;
import med.voll.api.entity.direccion.DatosDireccion;

public record DatosActualizacaoMedico(
        @NotNull
        Long id,
        String nombre,
        String telefono,
        DatosDireccion direccion) {
}
