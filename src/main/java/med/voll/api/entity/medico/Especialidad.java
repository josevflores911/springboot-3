package med.voll.api.entity.medico;

public enum Especialidad {

    ORTOPEDIA,
    CARDIOLOGIA,
    GINECOLOGIA,
    DERMATOLOGIA
}
