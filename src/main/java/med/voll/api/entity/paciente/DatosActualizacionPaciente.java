package med.voll.api.entity.paciente;

import jakarta.validation.constraints.NotNull;
import med.voll.api.entity.direccion.DatosDireccion;

public record DatosActualizacionPaciente(
        @NotNull
        Long id,
        String nombre,
        String telefono,
        DatosDireccion direccion) {
}
