package med.voll.api.entity.paciente;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import med.voll.api.entity.direccion.Direccion;

@Table(name = "pacientes")
@Entity(name = "Paciente")
@Getter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "id")
public class Paciente {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String nombre;
    private String email;

    private String telefono;

    private String documento;

    @Embedded
    private Direccion direccion;

    private Boolean activo;

    public Paciente(DatosRegistroPaciente datos) {
        this.activo = true;
        this.nombre = datos.nombre();
        this.email = datos.email();
        this.telefono = datos.telefono();
        this.documento = datos.documento();
        this.direccion = new Direccion(datos.direccion());
    }

    public void atualizarInformacoes(DatosActualizacionPaciente dados) {
        if (dados.nombre() != null) {
            this.nombre = dados.nombre();
        }
        if (dados.telefono() != null) {
            this.telefono = dados.telefono();
        }
        if (dados.direccion() != null) {
            this.direccion.actualizarDireccion(dados.direccion());
        }

    }

    public void excluir() {
        this.activo = false;
    }
}
