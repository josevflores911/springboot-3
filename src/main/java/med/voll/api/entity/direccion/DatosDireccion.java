package med.voll.api.entity.direccion;

import jakarta.validation.constraints.NotBlank;

public record DatosDireccion(
        @NotBlank
        String calle,
        @NotBlank
        String barrio,
        @NotBlank
        //@Pattern(regexp="\\d{8}")
        String codigoPostal,
        @NotBlank
        String ciudad,
        @NotBlank
        String estado,
        String numero,
        String complemento) {
}
