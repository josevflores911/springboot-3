package med.voll.api.entity.direccion;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Embeddable
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Direccion {

    private String calle;
    private String barrio;
    private String codigoPostal;
    private String numero;
    private String complemento;
    private String ciudad;
    private String estado;

    public Direccion(DatosDireccion datosDireccion) {
        this.calle=datosDireccion.calle();
        this.barrio=datosDireccion.barrio();
        this.codigoPostal=datosDireccion.codigoPostal();
        this.numero=datosDireccion.numero();
        this.complemento=datosDireccion.complemento();
        this.ciudad=datosDireccion.ciudad();
        this.estado=datosDireccion.estado();

    }

    public void actualizarDireccion(DatosDireccion direccion) {
        if(direccion.calle()!=null)
            this.calle=direccion.calle();
        if(direccion.barrio()!=null)
            this.barrio=direccion.barrio();
        if(direccion.codigoPostal()!=null)
            this.codigoPostal=direccion.codigoPostal();
        if(direccion.numero()!=null)
            this.numero=direccion.numero();
        if(direccion.complemento()!=null)
            this.complemento=direccion.complemento();
        if(direccion.ciudad()!=null)
            this.ciudad=direccion.ciudad();
        if(direccion.estado()!=null)
            this.estado=direccion.estado();
    }
}
