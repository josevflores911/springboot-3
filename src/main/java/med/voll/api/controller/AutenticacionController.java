package med.voll.api.controller;

import jakarta.validation.Valid;
import med.voll.api.entity.usuario.DatosAutenticacion;
import med.voll.api.entity.usuario.Usuario;
import med.voll.api.infra.security.DatosTokenJWT;
import med.voll.api.infra.security.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/login")
public class AutenticacionController {

    @Autowired
    private AuthenticationManager manager;

    @Autowired
    private TokenService tokenService;

    @PostMapping
    public ResponseEntity efectuarLogin(@RequestBody @Valid DatosAutenticacion datos){

        System.out.println("datos.login()");
        System.out.println(datos.login());
        var authenticationToken = new UsernamePasswordAuthenticationToken(datos.login(),datos.password());
        var authentication = manager.authenticate(authenticationToken);

        var tokenJWT=tokenService.gerarToken((Usuario)authentication.getPrincipal());

        return ResponseEntity.ok(new DatosTokenJWT(tokenJWT));
    }

}
