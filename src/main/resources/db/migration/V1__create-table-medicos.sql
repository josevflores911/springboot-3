
create table medicos(

    id bigint not null auto_increment,
    nombre varchar(100) not null,
    email varchar(100) not null unique,
    crm varchar(6) not null unique,
    especialidad varchar(100) not null,
    calle varchar(100) not null,
    barrio varchar(100) not null,
    codigo_postal varchar(9) not null,
    complemento varchar(100),
    numero varchar(20),
    estado char(2) not null,
    ciudad varchar(100) not null,

    primary key(id)

);
--delete from flyway_schema_history where success = 0;
--drop table flyway_schema_history
--create database flyway_schema_history
--DROP DATABASE IF EXISTS 'vollmed_api';